#!/bin/sh

# Change directory to www/ directory.
cd "$(dirname "$0")/www/" || exit
echo "Serving $(pwd)"
# From www/
python -m http.server --bind localhost 8085
