# Contra Dance Renderer

This takes contra dances in the JSON format used by https://contradb.com/
and displays a visualization of the dance on a web page. Currently very
early in development and only works on a single dance, Isaac Banner's
["Two Hearts in Time"](https://contradb.com/dances/2014), chosen as a
simple dance (i.e. single progression improper, no shadow or
next/previous neighbor interactions).


## Development

To develop locally, you need
[TypeScript](https://www.typescriptlang.org/download/) installed. You
can check if you have the **`tsc`** (TypeScript Compiler) command available.

While developing, leave the [watch-tsc.sh](./watch-tsc.sh) script running
or, equivalently, run the following the command from the root of the
repository:
```sh
tsc --watch
```

Also, serve the `www/` directory from a local webserver.
The [serve.sh](./serve.sh) script will do this or you can run the
following command from the `www/` directory of the repository:
```sh
python -m http.server --bind localhost 8085
```
(Nothing special about [Python](https://www.python.org/downloads/) here,
just the easiest web server to set up.)

Then open http://localhost:8085/ in a web browser. The site should work
in any modern browser (tested in Firefox and Chromium).

Any text editor/IDE works, but I find
[VS Code](https://code.visualstudio.com/Download)'s
[TypeScript support](https://code.visualstudio.com/Docs/languages/typescript)
works well.
