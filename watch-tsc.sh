#!/bin/sh

# Change directory to repo root.
cd "$(dirname "$0")" || exit
echo "Watching $(pwd)"
# From /
tsc --watch
