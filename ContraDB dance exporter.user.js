// ==UserScript==
// @name     ContraDB dance exporter
// @version  1
// @grant    GM.setClipboard
// @include        https://contradb.com/dances/new?copy_dance_id=*
// ==/UserScript==

const obj = Object.fromEntries(
      [...document.forms[0].elements]
      .filter(el => el.name.startsWith('dance[') && el.name != 'dance[publish]')
      .map(el => ([el.name.substring(6, el.name.length - 1), el.value]))
      .map(el => el[0] === 'figures_json'
           ? ['figures', JSON.parse(el[1])]
           : el[0] === 'title' && el[1].endsWith(' variation')
           ? [el[0], el[1].substring(0, el[1].length - ' variation'.length)]
           : el));
obj.id = parseInt(document.forms[0].id.substring('new_dance_'.length));
GM.setClipboard(JSON.stringify(obj) + ',\n');
alert('Dance copied to clipboard.')
