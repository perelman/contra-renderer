import { SemanticAnimationKind } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

class CustomSingleVariant extends SingleVariantMoveInterpreter<Custom, "custom"> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    // TODO refactor so this is in separate classes?
    if (this.move.parameters.custom.includes("mirrored mad robin")) {
      return this.handleCircleMove(({ id, startPos }) => {
        // TODO Read custom to decide direction?
        const startAndEndPos = {
          ...startPos,
          facing: startPos.which.facingAcross(),
          hands: undefined,
        };
        return this.combine([{
          beats: this.move.beats,
          startPosition: startAndEndPos,
          endPosition: startAndEndPos,
          movementPattern: {
            kind: SemanticAnimationKind.DoSiDo,
            amount: startPos.which.isLeft() ? -360 : 360,
            around: startPos.which.leftRightSide(),
          },
        }]);
      });
    } else {
      return this.errorStandStill();
    }
  }
}

class Custom extends MoveInterpreter<"custom"> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new CustomSingleVariant(this, startingPos);
  }
}

moveInterpreters.set("custom", Custom);