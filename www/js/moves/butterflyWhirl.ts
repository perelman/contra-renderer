import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "butterfly whirl";

class ButterflyWhirlSingleVariant extends SingleVariantMoveInterpreter<ButterflyWhirl, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    return this.handleCircleMove(({ startPos }) => {
      return this.combine([{
        beats: this.move.beats,
        endPosition: startPos,
        movementPattern: {
          kind: SemanticAnimationKind.RotateAround,
          around: startPos.which.leftRightSide(),
          // TODO hand around isn't the same as allemande...
          byHand: startPos.which.isOnLeftLookingAcross() ? Hand.Right : Hand.Left,
          close: true,
          minAmount: 360,
        }
      }], startPos);
    });
  }
}

class ButterflyWhirl extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new ButterflyWhirlSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, ButterflyWhirl);