import { HandConnection, HandTo, CircleSide, Facing } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "California twirl";

class CaliforniaTwirlSingleVariant extends SingleVariantMoveInterpreter<CaliforniaTwirl, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    return this.handleCirclePairedMove(this.move.parameters.who, ({ startPos }) => {
      // TODO does "who" matter here or is it entirely positional? At least need to know who to omit.

      const onLeft: boolean = startPos.which.isOnLeftLookingUpAndDown();
      // TODO get rid of this 1 beat set up and make it part of TwirlSwap?
      return this.combine([
        {
          beats: 1,
          endPosition: {
            ...startPos,
            hands: new Map<Hand, HandConnection>([onLeft
              ? [Hand.Right, { to: HandTo.DancerRight, hand: Hand.Left }]
              : [Hand.Left, { to: HandTo.DancerLeft, hand: Hand.Right }]]),
            facing: startPos.which.topBottomSide() === CircleSide.Top ? Facing.Down : Facing.Up,
          },
          movementPattern: {
            kind: SemanticAnimationKind.Linear,
          }
        },
        {
          beats: this.move.beats - 1,
          endPosition: {
            ...startPos,
            which: startPos.which.swapAcross(),
            facing: startPos.which.topBottomSide() === CircleSide.Top ? Facing.Up : Facing.Down,
          },
          movementPattern: {
            kind: SemanticAnimationKind.TwirlSwap,
            around: startPos.which.topBottomSide(),
            hand: onLeft ? Hand.Right : Hand.Left,
          }
        }], startPos);
    });
  }
}

class CaliforniaTwirl extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new CaliforniaTwirlSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, CaliforniaTwirl);