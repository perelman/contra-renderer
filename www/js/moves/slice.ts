import { SemanticPosition, PositionKind, handToDancerToSideInCircleFacingAcross, LongLines } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, PartialLowLevelMove, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "slice";

class SliceSingleVariant extends SingleVariantMoveInterpreter<Slice, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    if (this.move.parameters["slice increment"] === "dancer") {
      // TODO Maybe this only actually gets used to move an entire couple by going diagonal back?
      throw new Error("Slicing by a single dancer is unsupported.");
    }

    const sliceLeft = this.move.parameters.slide;
    const sliceReturns = this.move.parameters["slice return"] !== "none";

    const sliceForwardBeats = sliceReturns ? this.move.beats / 2 : this.move.beats;
    const sliceBackwardBeats = this.move.beats - sliceForwardBeats;

    return this.handleCircleMove(({ startPos }) => {
      const startingPos: SemanticPosition & { setOffset: number } = {
        kind: PositionKind.Circle,
        which: startPos.which,
        facing: startPos.which.facingAcross(),
        hands: handToDancerToSideInCircleFacingAcross(startPos.which),
        setOffset: startPos.setOffset ?? 0,
        lineOffset: startPos.lineOffset,
      };
      const sliceAmount = startingPos.which.isLeft() === sliceLeft ? +0.5 : -0.5;
      const forwardOffset = startingPos.setOffset + sliceAmount;
      const endOffset = this.move.parameters["slice return"] === "diagonal" ? forwardOffset + sliceAmount : forwardOffset;

      const sliceForward: PartialLowLevelMove = {
        beats: sliceForwardBeats,
        endPosition: { ...startingPos, setOffset: forwardOffset, longLines: LongLines.Forward },
        movementPattern: { kind: SemanticAnimationKind.Linear },
      };
      const maybeSliceBackward: PartialLowLevelMove[] = sliceReturns ? [{
        beats: sliceBackwardBeats,
        endPosition: { ...startingPos, setOffset: endOffset },
        movementPattern: { kind: SemanticAnimationKind.Linear },
      }] : [];

      return this.combine([sliceForward, ...maybeSliceBackward], startingPos);
    });
  }
}

class Slice extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new SliceSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, Slice);