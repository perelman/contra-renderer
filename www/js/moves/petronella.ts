import { Facing, SemanticPosition } from "../interpreterCommon.js";
import { SemanticAnimationKind, LowLevelMove } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, PartialLowLevelMove, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";
import { balanceCircleInAndOut } from "./balanceTheRing.js";

class PetronellaSingleVariant extends SingleVariantMoveInterpreter<Petronella, "petronella"> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    // TODO These should be actual parameters, not parsing the notes...
    const rightShoulder: boolean = !(this.move.note?.includes('left') ?? false);
    const newCircle: boolean = this.move.note?.includes('end facing') ?? this.move.progression;

    return this.handleCircleMove(({ startPos }) => {
      let finalPosition = {
        ...startPos,
        facing: Facing.CenterOfCircle,
        which: startPos.which.circleRight(rightShoulder ? 1 : -1),
        hands: undefined,
      };

      if (newCircle) {
        finalPosition = {
          ...finalPosition,
          which: finalPosition.which.swapUpAndDown(),
          setOffset: (finalPosition.setOffset ?? 0) + (finalPosition.which.isTop() ? -0.5 : +0.5),
        }
      }

      const spin: ((prevEnd: SemanticPosition) => PartialLowLevelMove) = prevEnd => ({
        beats: this.move.beats - (this.move.parameters.bal ? 4 : 0),
        startPosition: {
          ...prevEnd,
          facing: Facing.CenterOfCircle,
          hands: undefined,
        },
        endPosition: finalPosition,
        movementPattern: {
          kind: SemanticAnimationKind.Linear,
          minRotation: rightShoulder ? 180 : -180,
          handsDuring: "None",
        },
      });

      if (this.move.parameters.bal) {
        const balance: LowLevelMove[] = balanceCircleInAndOut(this.move, startPos);
        return PetronellaSingleVariant.append([...balance], spin);
      } else {
        return this.combine([spin]);
      }
    });
  }
}

class Petronella extends MoveInterpreter<"petronella"> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new PetronellaSingleVariant(this, startingPos);
  }
}

moveInterpreters.set("petronella", Petronella);