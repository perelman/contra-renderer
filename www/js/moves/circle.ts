import { Facing, handsInCircle } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "circle";

class CircleSingleVariant extends SingleVariantMoveInterpreter<Circle, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    return this.handleCircleMove(({ startPos }) => {
      const places = this.move.parameters.places / 90 * (this.move.parameters.turn ? 1 : -1);

      return this.combine([
        {
          beats: this.move.beats,
          endPosition: {
            ...startPos,
            facing: Facing.CenterOfCircle,
            hands: handsInCircle,
            which: startPos.which.circleLeft(places),
          },
          movementPattern: {
            kind: SemanticAnimationKind.Circle,
            places,
          }
        },
      ], { ...startPos, facing: Facing.CenterOfCircle });
    });
  }
}

class Circle extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new CircleSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, Circle);