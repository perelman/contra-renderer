import { DancerDistance, handToDancerToSideInCircleFacingAcross } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "star promenade";

class StarPromenadeSingleVariant extends SingleVariantMoveInterpreter<StarPromenade, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    const starPromenadeHand = this.move.parameters.hand ? Hand.Right : Hand.Left;
    const starPromenadeSwap = (this.move.parameters.circling % 360) === 180;
    if (!starPromenadeSwap && (this.move.parameters.circling % 360 !== 0)) {
      throw new Error(this.move.move + " circling by not a multiple of 180 degrees is unsupported.");
    }

    // TODO start promenade hands/show dancers close
    return this.handleCircleMove(({ id, startPos }) => {
      const inCenter = this.findPairOpposite(this.move.parameters.who, id) !== null;
      // TODO Actually, does star promenade end facing out and butterfly whirl swaps?
      const endWhich = starPromenadeSwap ? startPos.which.swapDiagonal() : startPos.which;
      const endFacing = endWhich.facingAcross();
      return this.combine([{
        beats: this.move.beats,
        endPosition: {
          ...startPos,
          which: endWhich,
          facing: endFacing,
          dancerDistance: DancerDistance.Compact,
          // TODO Perhaps different hands indication for "scooped"?
          hands: handToDancerToSideInCircleFacingAcross(endWhich),
        },
        movementPattern: {
          kind: SemanticAnimationKind.RotateAround,
          around: "Center",
          byHand: inCenter ? starPromenadeHand : undefined,
          close: inCenter,
          minAmount: this.move.parameters.hand ? this.move.parameters.circling : -this.move.parameters.circling,
        }
      }], startPos);
    });
  }
}

class StarPromenade extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new StarPromenadeSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, StarPromenade);