import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "right left through";

class RightLeftThroughSingleVariant extends SingleVariantMoveInterpreter<RightLeftThrough, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    if (this.move.parameters.dir !== "across") {
      throw new Error(this.move.move + " with dir " + this.move.parameters.dir + " is unsupported.");
    }

    return this.handleCircleMove(({ startPos }) => {
      const startingPos = { ...startPos, facing: startPos.which.facingAcross() };
      const swappedPos = { ...startingPos, which: startingPos.which.swapAcross() };
      return this.combine([
        {
          beats: this.move.beats / 2,
          endPosition: swappedPos,
          movementPattern: {
            kind: SemanticAnimationKind.PassBy,
            side: Hand.Right,
            withHands: true,
            facing: "Start",
            around: startingPos.which.topBottomSide(),
            otherPath: "Swap",
          },
        },
        {
          beats: this.move.beats / 2,
          endPosition: {
            ...startingPos,
            which: startingPos.which.swapDiagonal(),
            facing: startingPos.which.facingOut()
          },
          movementPattern: {
            kind: SemanticAnimationKind.CourtesyTurn,
          },
        }
      ], startingPos);
    });
  }
}

class RightLeftThrough extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new RightLeftThroughSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, RightLeftThrough);