import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "pass by";

class PassBySingleVariant extends SingleVariantMoveInterpreter<PassBy, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    const passByShoulder = this.move.parameters.shoulder ? Hand.Left : Hand.Right;
    return this.handlePairedMove(this.move.parameters.who, ({ startPos, around, withPos }) => {
      return this.combine([{
        beats: this.move.beats,
        // TODO Is pass by always a swap?
        endPosition: withPos,
        movementPattern: {
          kind: SemanticAnimationKind.PassBy,
          around,
          facing: "Forward",
          otherPath: "Swap",
          side: passByShoulder,
          withHands: false,
        }
      }], startPos);
    });
  }
}

class PassBy extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new PassBySingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, PassBy);