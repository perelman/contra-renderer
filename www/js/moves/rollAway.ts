import { DanceRole, CoupleRole } from "../danceCommon.js";
import { PositionKind } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "roll away";

class RollAwaySingleVariant extends SingleVariantMoveInterpreter<RollAway, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    // TODO maybe can roll away in short lines?
    return this.handleCirclePairedMove(this.move.parameters.who, ({ id, startPos, withPos }) => {
      let isRoller: boolean;
      switch (this.move.parameters.who) {
        case "gentlespoons":
          isRoller = id.danceRole === DanceRole.Lark;
          break;
        case "ladles":
          isRoller = id.danceRole === DanceRole.Robin;
          break;
        case "ones":
          isRoller = id.coupleRole === CoupleRole.Ones;
          break;
        case "twos":
          isRoller = id.coupleRole === CoupleRole.Twos;
          break;
        case "first corners":
        case "second corners":
          throw "Roll away in contra corners is unsupported.";
      }

      // TODO This isn't quite right if there's no 1/2 sash?
      let swapPos = withPos;
      if (swapPos.kind === PositionKind.Circle && swapPos.longLines) {
        swapPos = { ...swapPos, longLines: undefined };
      }

      // TODO animate hands?
      if (isRoller) {
        if (this.move.parameters["½sash"]) {
          // swap positions by sliding
          return this.combine([{
            beats: this.move.beats,
            endPosition: swapPos,
            movementPattern: { kind: SemanticAnimationKind.Linear },
          }], startPos);
        } else {
          // just stand still
          return this.combine([{
            beats: this.move.beats,
            endPosition: { ...startPos, longLines: undefined },
            movementPattern: { kind: SemanticAnimationKind.Linear },
          }], startPos);
        }
      } else {
        // being rolled away, so do a spin
        return this.combine([{
          beats: this.move.beats,
          // TODO Is this the right end position logic?
          endPosition: this.move.parameters["½sash"] ? swapPos : { ...startPos, which: startPos.which.swapDiagonal() },
          movementPattern: { kind: SemanticAnimationKind.RollAway },
        }], startPos);
      }
    });
  }
}

class RollAway extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new RollAwaySingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, RollAway);