import { SemanticPosition, PositionKind, handsInLine } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "form long waves";

class FormLongWavesSingleVariant extends SingleVariantMoveInterpreter<FormLongWaves, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    // TODO A zero beat move should just be selecting a variant? Or maybe not because this just changes facing/hands.
    if (this.move.beats !== 0) {
      throw new Error(this.move.move + " unsupported except for zero beats marking end of previous move.");
    }
    return this.handleCircleMove(({ id, startPos }) => {
      const facingIn = this.findPairOpposite(this.move.parameters.who, id) !== null;

      const startAndEndPos: SemanticPosition = {
        kind: PositionKind.Circle,
        which: startPos.which,
        facing: facingIn ? startPos.which.facingAcross() : startPos.which.facingOut(),
        hands: handsInLine({ wavy: true, which: startPos.which }),
        setOffset: startPos.setOffset,
        lineOffset: startPos.lineOffset,
      }

      return this.combine([{
        beats: this.move.beats,
        endPosition: startAndEndPos,
        movementPattern: { kind: SemanticAnimationKind.StandStill },
      }
      ], startAndEndPos);
    });
  }
}

class FormLongWaves extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new FormLongWavesSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, FormLongWaves);