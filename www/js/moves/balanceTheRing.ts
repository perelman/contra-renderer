import { BalanceWeight, Facing, HandConnection, HandTo, PositionKind, SemanticPosition } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { LowLevelMove, SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";


export function balanceCircleInAndOut(move: Move, startPos: SemanticPosition, balanceBeats?: number): [LowLevelMove, LowLevelMove] {
  if (startPos.kind !== PositionKind.Circle) {
    throw "Balance circle must start in a circle, but starting at " + startPos;
  }

  balanceBeats ??= 4;
  const balancePartBeats = balanceBeats/2;

  const holdingHandsInCircle: SemanticPosition = {...startPos,
    facing: Facing.CenterOfCircle,
    hands: new Map<Hand, HandConnection>([
      [Hand.Left, { hand: Hand.Right, to: HandTo.LeftInCircle }],
      [Hand.Right, { hand: Hand.Left, to: HandTo.RightInCircle }],
    ]),
  };
  const circleBalancedIn: SemanticPosition = {...holdingHandsInCircle,
    balance: BalanceWeight.Forward,
  };

  const balanceIn: LowLevelMove = {
    move,
    startBeat: 0,
    beats: balancePartBeats,
    startPosition: holdingHandsInCircle,
    endPosition: circleBalancedIn,
    movementPattern: { kind: SemanticAnimationKind.Linear },
  };
  const balanceOut: LowLevelMove = {...balanceIn,
    startBeat: balancePartBeats,
    startPosition: circleBalancedIn,
    endPosition: holdingHandsInCircle,
  };

  return [balanceIn, balanceOut];
}

class BalanceTheRingSingleVariant extends SingleVariantMoveInterpreter<BalanceTheRing, "balance the ring"> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    return this.handleCircleMove(({ startPos }) => balanceCircleInAndOut(this.move, startPos));
  }
}

class BalanceTheRing extends MoveInterpreter<"balance the ring"> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new BalanceTheRingSingleVariant(this, startingPos);
  }
}

moveInterpreters.set("balance the ring", BalanceTheRing);
