import { SemanticPosition, Facing, HandConnection, HandTo, LongLines } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "long lines";

class LongLinesSingleVariant extends SingleVariantMoveInterpreter<LongLinesInterpreter, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    return this.handleCircleMove(({ startPos }) => {
      const startPosition: SemanticPosition = {
        ...startPos,
        longLines: undefined,
        // TODO Occassionally dances have long lines facing out. This will get that wrong.
        facing: startPos.which.isLeft() ? Facing.Right : Facing.Left,
        hands: new Map<Hand, HandConnection>([
          [Hand.Left, { hand: Hand.Right, to: HandTo.DancerLeft }],
          [Hand.Right, { hand: Hand.Left, to: HandTo.DancerRight }],
        ]),
      };

      if (this.move.parameters.go) {
        const forwardBeats = this.move.beats / 2;
        const backwardBeats = this.move.beats - forwardBeats;
        return this.combine([
          {
            beats: forwardBeats,
            endPosition: { ...startPosition, longLines: LongLines.Forward },
            movementPattern: { kind: SemanticAnimationKind.Linear },
          },
          {
            beats: backwardBeats,
            endPosition: startPosition,
            movementPattern: { kind: SemanticAnimationKind.Linear, minRotation: 0 },
          },
        ], startPosition);
      } else {
        return this.combine([{
          beats: this.move.beats,
          endPosition: { ...startPosition, longLines: LongLines.Forward },
          movementPattern: { kind: SemanticAnimationKind.Linear },
        }], startPosition);
      }
    });
  }
}

class LongLinesInterpreter extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new LongLinesSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, LongLinesInterpreter);