import { BalanceWeight } from "../interpreterCommon.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, MoveInterpreterCtorArgs, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

class BalanceSingleVariant extends SingleVariantMoveInterpreter<Balance, "balance"> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    return this.handleMove(({ startPos }) => {
      // TODO Use who to determine facing?
      // TODO Could be left to right, not back and forth?
      // TODO How to determine hand... by next move, I guess?

      return this.combine([
        {
          beats: this.moveInterpreter.forwardBeats,
          endPosition: { ...startPos, balance: BalanceWeight.Forward },
          movementPattern: { kind: SemanticAnimationKind.Linear },
        },
        {
          beats: this.moveInterpreter.backwardBeats,
          endPosition: { ...startPos, balance: BalanceWeight.Backward },
          movementPattern: { kind: SemanticAnimationKind.Linear },
        },
      ], startPos);
    });
  }
}

class Balance extends MoveInterpreter<"balance"> {
  public readonly forwardBeats: number;
  public readonly backwardBeats: number;

  constructor(args: MoveInterpreterCtorArgs<"balance">) {
    super(args);

    this.forwardBeats = this.move.beats / 2;
    this.backwardBeats = this.move.beats - this.forwardBeats;
  }

  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new BalanceSingleVariant(this, startingPos);
  }
}

moveInterpreters.set("balance", Balance);
