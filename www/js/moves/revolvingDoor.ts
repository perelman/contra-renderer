import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "revolving door";

class RevolvingDoorSingleVariant extends SingleVariantMoveInterpreter<RevolvingDoor, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    const byHand = this.move.parameters.hand ? Hand.Right : Hand.Left;
    // TODO More parts? Or define an animation kind?
    const waitBeats = 2;
    const carryBeats = this.move.beats / 2;
    const returnBeats = this.move.beats - carryBeats - waitBeats;
    return this.handleCirclePairedMove(this.move.parameters.whom, ({ id, startPos }) => {
      const isCarried = this.findPairOpposite(this.move.parameters.who, id) === null;
      const startingPos = { ...startPos, hands: undefined };

      // TODO animation here needs work.
      if (isCarried) {
        const endWhich = startPos.which.swapDiagonal();
        return this.combine([
          prevEnd => ({
            beats: waitBeats,
            endPosition: prevEnd,
            movementPattern: { kind: SemanticAnimationKind.StandStill },
          }),
          {
            beats: carryBeats,
            endPosition: {
              ...startPos,
              which: endWhich,
              facing: endWhich.facingAcross(),
            },
            movementPattern: {
              kind: SemanticAnimationKind.RotateAround,
              minAmount: byHand === Hand.Right ? 180 : -180,
              around: "Center",
              byHand,
              close: false,
            },
          },
          prevEnd => ({
            beats: returnBeats,
            endPosition: prevEnd,
            movementPattern: { kind: SemanticAnimationKind.StandStill },
          }),
        ], startingPos);
      } else {
        return this.combine([
          {
            beats: this.move.beats,
            endPosition: startPos,
            movementPattern: {
              kind: SemanticAnimationKind.RotateAround,
              minAmount: byHand === Hand.Right ? 180 : -180,
              around: "Center",
              byHand,
              close: true,
            },
          },
        ], startingPos);
      }
    });
  }
}

class RevolvingDoor extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new RevolvingDoorSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, RevolvingDoor);