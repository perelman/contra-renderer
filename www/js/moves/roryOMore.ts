import { PositionKind, SemanticPosition, Facing, BalanceWeight, handsInLine } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, PartialLowLevelMove, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "Rory O'More";
class RoryOMoreSingleVariant extends SingleVariantMoveInterpreter<RoryOMore, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    if (this.move.parameters.who !== "everyone") {
      throw new Error(this.move.move + " that doesn't include everyone is unsupported.");
    }

    // TODO Could be in long or short lines.
    const roryDir = this.move.parameters.slide ? Hand.Left : Hand.Right;
    const balBeats = this.move.parameters.bal ? this.move.beats / 2 : 0;
    const balPartBeats = balBeats / 2;
    const roryBeats = this.move.beats - balBeats;
    return this.handleMove(({ startPos }) => {
      const isShortLines: boolean = startPos.kind === PositionKind.ShortLines;

      const startingPos: SemanticPosition = {
        ...startPos,
        hands: handsInLine({ wavy: true, which: startPos.which, facing: startPos.facing })
      };

      let endPos: SemanticPosition;
      if (startPos.kind === PositionKind.ShortLines) {
        if (startPos.facing !== Facing.Up && startPos.facing !== Facing.Down) {
          throw new Error("To be in wavy lines, must be facing up or down, not " + startPos.facing);
        }
        const endWhich = startPos.which.shift(roryDir, startPos.facing);
        endPos = {
          ...startPos,
          which: endWhich,
          hands: handsInLine({ wavy: true, which: endWhich, facing: startPos.facing })
        };
      } else {
        throw new Error(this.move.move + " is currently only supported in short lines.");
      }

      const maybeBalance: PartialLowLevelMove[] = (this.move.parameters.bal ? [
        {
          beats: balPartBeats,
          endPosition: { ...startingPos, balance: roryDir === Hand.Left ? BalanceWeight.Left : BalanceWeight.Right },
          movementPattern: { kind: SemanticAnimationKind.Linear },
        },
        {
          beats: balPartBeats,
          endPosition: { ...startingPos, balance: roryDir === Hand.Left ? BalanceWeight.Right : BalanceWeight.Left },
          movementPattern: { kind: SemanticAnimationKind.Linear },
        },
      ] : []);

      return this.combine([...maybeBalance,
      {
        beats: roryBeats,
        endPosition: endPos,
        movementPattern: {
          kind: SemanticAnimationKind.Linear,
          minRotation: roryDir === Hand.Right ? +360 : -360,
          handsDuring: "None",
        },
      }
      ], startingPos);
    });
  }
}

class RoryOMore extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new RoryOMoreSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, RoryOMore);