import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName : Move["move"] = "slide along set";

class SlideAlongSetSingleVariant extends SingleVariantMoveInterpreter<SlideAlongSet, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
      const slideLeft = this.move.parameters.slide;

      return this.handleCircleMove(({ startPos }) => {
        const startingPos = {
          ...startPos,
          facing: startPos.which.facingAcross(),
        };

        return this.combine([{
          beats: this.move.beats,
          endPosition: {
            ...startingPos,
            setOffset: (startingPos.setOffset ?? 0) + (startingPos.which.isLeft() === slideLeft ? +0.5 : -0.5),
          },
          movementPattern: { kind: SemanticAnimationKind.Linear },
        }], startingPos);
      });
  }
}

class SlideAlongSet extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new SlideAlongSetSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, SlideAlongSet);