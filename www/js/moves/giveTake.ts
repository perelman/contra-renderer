import { LongLines, HandConnection, HandTo } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, PartialLowLevelMove, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "give & take";

class GiveTakeSingleVariant extends SingleVariantMoveInterpreter<GiveTake, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    const give: boolean = this.move.parameters.give;
    const takeToSide = this.move.parameters.who;

    const giveBeats = give ? this.move.beats / 2 : 0;
    const takeBeats = this.move.beats - giveBeats;

    return this.handleCircleMove(({ id, startPos }) => {
      const isTaker = this.findPairOpposite(takeToSide, id) !== null;

      const maybeGive: [] | [PartialLowLevelMove] = give ? [{
        beats: giveBeats,
        endPosition: { ...startPos, longLines: LongLines.Forward },
        movementPattern: { kind: SemanticAnimationKind.Linear, minRotation: 0 },
      }] : [];
      const takeHands = new Map<Hand, HandConnection>([
        [Hand.Left, { hand: Hand.Right, to: HandTo.DancerForward }],
        [Hand.Right, { hand: Hand.Left, to: HandTo.DancerForward }],
      ]);

      return this.combine([...maybeGive, (prevEnd) => ({
        beats: takeBeats,
        startPosition: { ...prevEnd, hands: undefined },
        endPosition: isTaker ? {
          ...startPos,
          which: startPos.which,
          longLines: undefined,
          hands: takeHands,
        } : {
          ...startPos,
          which: startPos.which.swapAcross(),
          longLines: LongLines.Near,
          hands: takeHands,
        },
        movementPattern: { kind: SemanticAnimationKind.Linear, minRotation: 0 },
      })], startPos);
    });
  }
}

class GiveTake extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new GiveTakeSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, GiveTake);