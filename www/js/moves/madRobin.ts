import { SemanticPosition, PositionKind } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "mad robin";

class MadRobinSingleVariant extends SingleVariantMoveInterpreter<MadRobin, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    if (this.move.parameters.circling !== 360) {
      throw new Error("mad robin circling not exactly once is unsupported.");
    }

    return this.handleCircleMove(({ id, startPos }) => {
      // Read who of mad robin to decide direction.
      const madRobinClockwise: boolean = (this.findPairOpposite(this.move.parameters.who, id) !== null) === startPos.which.isOnLeftLookingAcross();

      const startAndEndPos: SemanticPosition = {
        kind: PositionKind.Circle,
        which: startPos.which,
        facing: startPos.which.facingAcross(),
        setOffset: startPos.setOffset,
        lineOffset: startPos.lineOffset,
      }

      return this.combine([{
        beats: this.move.beats,
        startPosition: startAndEndPos,
        endPosition: startAndEndPos,
        movementPattern: {
          kind: SemanticAnimationKind.DoSiDo,
          amount: madRobinClockwise ? this.move.parameters.circling : -this.move.parameters.circling,
          around: startPos.which.leftRightSide(),
        },
      }]);
    });
  }
}

class MadRobin extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new MadRobinSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, MadRobin);