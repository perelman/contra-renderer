import { HandConnection, HandTo, BalanceWeight } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, PartialLowLevelMove, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "pull by dancers";

class PullByDancersSingleVariant extends SingleVariantMoveInterpreter<PullByDancers, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    // TODO Might make sense to think of pull by as not a full swap?
    //  e.g., in Blue and Green Candles, it's treated as only getting to
    //        ShortLinesPosition.Middle* before doing an allemande.
    return this.handlePairedMove(this.move.parameters.who, ({ startPos, around, withPos }) => {
      const hand = this.move.parameters.hand ? Hand.Right : Hand.Left;
      const balanceBeats = this.move.parameters.bal
        ? this.move.beats > 4
          ? this.move.beats - 4
          : 2
        : 0;
      const balancePartBeats = balanceBeats / 2;
      const pullBeats = this.move.beats - balanceBeats;

      // TODO Adjust facing?
      const startPosition = {
        ...startPos,
        hands: new Map<Hand, HandConnection>([
          [
            hand,
            { hand, to: around === "Center" ? HandTo.DiagonalAcrossCircle : HandTo.DancerForward }
          ]])
      };

      const passBy: PartialLowLevelMove = {
        beats: pullBeats,
        endPosition: { ...withPos, facing: startPos.facing },
        movementPattern: {
          kind: SemanticAnimationKind.PassBy,
          around,
          side: hand,
          withHands: true,
          facing: "Start",
          otherPath: "Swap",
        }
      };

      if (this.move.parameters.bal) {
        return this.combine([
          {
            beats: balancePartBeats,
            endPosition: {
              ...startPosition,
              balance: BalanceWeight.Forward,
            },
            movementPattern: {
              kind: SemanticAnimationKind.Linear,
            }
          },
          {
            beats: balancePartBeats,
            endPosition: {
              ...startPosition,
              balance: BalanceWeight.Backward,
            },
            movementPattern: {
              kind: SemanticAnimationKind.Linear,
            }
          },
          passBy], startPosition);
      } else {
        return this.combine([passBy], startPosition);
      }
    });
  }
}

class PullByDancers extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new PullByDancersSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, PullByDancers);