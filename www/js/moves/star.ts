import { StarGrip, Facing } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "star";

class StarSingleVariant extends SingleVariantMoveInterpreter<Star, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    return this.handleCircleMove(({ startPos }) => {
      const hand = this.move.parameters.hand ? Hand.Right : Hand.Left;
      const grip = this.move.parameters.grip === "hands across"
        ? StarGrip.HandsAcross
        : this.move.parameters.grip === "wrist grip"
          ? StarGrip.WristGrip
          : undefined;
      const facing = hand === Hand.Left ? Facing.LeftInCircle : Facing.RightInCircle;

      const places = this.move.parameters.places / 90 * (hand === Hand.Right ? 1 : -1);

      return this.combine([
        {
          beats: this.move.beats,
          endPosition: {
            ...startPos,
            facing,
            hands: undefined,
            which: startPos.which.circleLeft(places),
          },
          movementPattern: {
            kind: SemanticAnimationKind.Star,
            hand,
            grip,
            places,
          }
        },
      ], { ...startPos, facing });
    });
  }
}

class Star extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new StarSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, Star);