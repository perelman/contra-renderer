import { SemanticPosition } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "turn alone";

class TurnAloneSingleVariant extends SingleVariantMoveInterpreter<TurnAlone, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    if (this.move.parameters.who !== "everyone" || this.move.beats !== 0) {
      throw new Error("turn alone unsupported except for changing to new circle.");
    }
    return this.handleCircleMove(({ startPos }) => {
      const which = startPos.which.swapUpAndDown();
      const startAndEndPos: SemanticPosition = {
        ...startPos,
        which,
        facing: which.facingUpOrDown(),
        setOffset: (startPos.setOffset ?? 0) + (startPos.which.isTop() ? +0.5 : -0.5),
      }

      return this.combine([{
        beats: this.move.beats,
        endPosition: startAndEndPos,
        movementPattern: { kind: SemanticAnimationKind.StandStill },
      }], startAndEndPos);
    });
  }
}

class TurnAlone extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new TurnAloneSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, TurnAlone);