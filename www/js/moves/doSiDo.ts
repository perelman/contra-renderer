import { SemanticPosition, Facing, CircleSide, PositionKind } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "do si do";

class DoSiDoSingleVariant extends SingleVariantMoveInterpreter<DoSiDo, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    if (!this.move.parameters.shoulder) {
      throw new Error("do si do by left shoulder is unsupported.");
    }

    let doSiDoEndKind: "Start" | "Swap" | "ShortLinesLeft" | "ShortLinesRight";
    const doSiDoCircling = this.move.parameters.circling % 360;
    if (doSiDoCircling === 0) {
      doSiDoEndKind = "Start";
    } else if (doSiDoCircling === 180) {
      doSiDoEndKind = "Swap";
    } else if (doSiDoCircling === 90) {
      doSiDoEndKind = this.move.parameters.shoulder ? "ShortLinesLeft" : "ShortLinesRight";
    } else if (doSiDoCircling === 270) {
      doSiDoEndKind = this.move.parameters.shoulder ? "ShortLinesRight" : "ShortLinesLeft";
    } else {
      throw new Error("do si do by " + this.move.parameters.circling + " degrees is unsupported.");
    }

    return this.handleCirclePairedMove(this.move.parameters.who, ({ startPos, around, withPos }) => {
      // TODO Use other parameters?
      const startingPos: SemanticPosition = {
        ...startPos,
        hands: undefined,
        facing: around === "Center"
          ? Facing.CenterOfCircle
          : around === CircleSide.Left || around === CircleSide.Right
            ? startPos.which.facingUpOrDown()
            : startPos.which.facingAcross(),
      };

      let endPos: SemanticPosition;
      switch (doSiDoEndKind) {
        case "Start":
          endPos = startingPos;
          break;
        case "Swap":
          endPos = { ...withPos, facing: startingPos.facing };
          break;
        case "ShortLinesLeft":
        case "ShortLinesRight":
          endPos = {
            kind: PositionKind.ShortLines,
            which: startPos.which.toShortLines(doSiDoEndKind === "ShortLinesLeft" ? Hand.Left : Hand.Right),
            facing: startingPos.facing,
            setOffset: startingPos.setOffset,
            lineOffset: startingPos.lineOffset,
          }
      }

      return this.combine([{
        beats: this.move.beats,
        startPosition: startingPos,
        endPosition: endPos,
        movementPattern: {
          kind: SemanticAnimationKind.DoSiDo,
          amount: this.move.parameters.circling,
          around,
        },
      }]);
    });
  }
}

class DoSiDo extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new DoSiDoSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, DoSiDo);