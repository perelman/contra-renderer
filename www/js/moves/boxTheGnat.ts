import { HandConnection, HandTo, BalanceWeight } from "../interpreterCommon.js";
import { Move } from "../libfigureMapper.js";
import { SemanticAnimationKind } from "../lowLevelMove.js";
import { Hand } from "../rendererConstants.js";
import { ISingleVariantMoveInterpreter, LowLevelMovesForAllDancers, MoveInterpreter, PartialLowLevelMove, SemanticPositionsForAllDancers, SingleVariantMoveInterpreter, moveInterpreters } from "./_moveInterpreter.js";

const moveName: Move["move"] = "box the gnat";

class BoxTheGnatSingleVariant extends SingleVariantMoveInterpreter<BoxTheGnat, typeof moveName> {
  moveAsLowLevelMoves(): LowLevelMovesForAllDancers {
    return this.handlePairedMove(this.move.parameters.who, ({ startPos, around, withPos }) => {
      const hand = this.move.parameters.hand ? Hand.Right : Hand.Left;
      const balanceBeats = this.move.parameters.bal
        ? this.move.beats > 4
          ? this.move.beats - 4
          : 2
        : 0;
      const balancePartBeats = balanceBeats / 2;
      const twirlBeats = this.move.beats - balanceBeats;

      // TODO Adjust facing?
      const startPosition = { ...startPos, hands: new Map<Hand, HandConnection>([[hand, { hand, to: HandTo.DancerForward }]]) };

      if (around === "Center") {
        throw "TwirlSwap around center is unsupported.";
      }

      const twirl: PartialLowLevelMove = {
        beats: twirlBeats,
        endPosition: withPos,
        movementPattern: {
          kind: SemanticAnimationKind.TwirlSwap,
          around,
          hand,
        }
      };

      if (this.move.parameters.bal) {
        return this.combine([
          {
            beats: balancePartBeats,
            endPosition: {
              ...startPosition,
              balance: BalanceWeight.Forward,
            },
            movementPattern: {
              kind: SemanticAnimationKind.Linear,
            }
          },
          {
            beats: balancePartBeats,
            endPosition: {
              ...startPosition,
              balance: BalanceWeight.Backward,
            },
            movementPattern: {
              kind: SemanticAnimationKind.Linear,
            }
          },
          twirl], startPosition);
      } else {
        return this.combine([twirl], startPosition);
      }
    });
  }
}

class BoxTheGnat extends MoveInterpreter<typeof moveName> {
  buildSingleVariantMoveInterpreter(startingPos: SemanticPositionsForAllDancers): ISingleVariantMoveInterpreter {
    return new BoxTheGnatSingleVariant(this, startingPos);
  }
}

moveInterpreters.set(moveName, BoxTheGnat);